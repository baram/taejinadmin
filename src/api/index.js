import axios from 'axios';
import { setInterceptors } from './common/interceptors';

//1. HTTP Request & Response와 관련된 기본 설정
// const config = {
// 	baseUrl: 'http://localhost:28080/',
// };

//인스턴스 만들기
function createInstance() {
	const instance = axios.create({
		baseURL: process.env.VUE_APP_API_URL,
	});
	return setInterceptors(instance);
}

const instance = createInstance();

//브랜드 상세
function fetchBrand(brandSeq) {
	return instance.get('api/brand/' + brandSeq);
}

//분야 상세
function fetchcategory(categorySeq) {
	return instance.get('api/category/' + categorySeq);
}

//뉴스 상세
function fetchNews(newsSeq) {
	return instance.get('api/admin-news/' + newsSeq);
}
//채용공고 상세
function fetchRecruit(recruitSeq) {
	return instance.get('api/recruit/' + recruitSeq);
}

//브랜드 등록
function createBrand(brandData) {
	return instance.post('api/brand', brandData, {
		headersFormdata,
	});
}
//브랜드 수정
function editBrand(brandSeq, brandData) {
	return instance.post('api/brand/' + brandSeq, brandData, {
		headersFormdata,
	});
}
//브랜드 삭제
function deleteBrand(brandSeq) {
	return instance.delete('api/brand/deleteBrand/' + brandSeq);
}
//분야 등록
function createcategory(brandData) {
	return instance.post('api/category', JSON.stringify(brandData), {
		headers,
	});
}
//뉴스 등록
//JSON 형태
// function createdNews(newsData) {
// 	return instance.post('api/news/addNews', JSON.stringify(newsData), {
// 		headers,
// 	});
// }

//formdata 형태
function createdNews(newsData) {
	return instance.post('api/admin-news', newsData, {
		headersFormdata,
	});
}
//뉴스 수정
function editNews(newsSeq, newsData) {
	return instance.post('api/admin-news/' + newsSeq, newsData, {
		headersFormdata,
	});
}

//분야 수정
function editcategory(categorySeq, categoryData) {
	return instance.put('api/category/' + categorySeq, categoryData, {
		headers,
	});
}

// function editRecruit(recruitSeq, recruitData) {
// 	return instance.put('api/admin-recruit/' + recruitSeq, recruitData, {
// 		headers,
// 	});
// }

//채용공고등록
function createdRecruit(recruitData) {
	return instance.post('api/recruit', JSON.stringify(recruitData), {
		headers,
	});
}

//브랜드 리스트
function BrandList(page) {
	//localhost:28080/api/brand/brandList/13?page=123&size=1
	return instance.get(
		'api/brand/lists' +
			'?searchKeyword=' +
			page.searchKeyword +
			'&page=' +
			page.page +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
	// let form = new FormData();
	// form.append('searchKeyword', page.searchKeyword);
	// form.append('page', page.page);
	// form.append('size', page.size);
	// return instance.get('api/brand/brandList', form, { headers });
}
//브랜드 셀렉트
function BrandSelect() {
	return instance.get('api/brand/brand-register');
}

//분야 리스트
function categoryList(page) {
	return instance.get(
		'api/category/lists' +
			'?searchKeyword=' +
			page.searchKeyword +
			'&page=' +
			page.page +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
}

//SNS 리스트
function SnsList(page) {
	return instance.get(
		'api/sns/lists' +
			'?searchKeyword=' +
			page.searchKeyword +
			'&page=' +
			page.page +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
}
//SNS 등록
function createSns(snsData) {
	return instance.post('api/sns', snsData, {
		headersFormdata,
	});
}

//SNS 수정
function editSns(snsSeq, snsData) {
	return instance.post('api/sns/update-sns/' + snsSeq, snsData, {
		headersFormdata,
	});
}

//sns 상세
function fetchsns(snsSeq) {
	return instance.get('api/sns/' + snsSeq);
}
//뉴스 리스트
function NewsList(page) {
	//localhost:28080/api/brand/brandList/13?page=123&size=1
	return instance.get(
		'api/admin-news/lists' +
			'?searchKeyword=' +
			page.searchKeyword +
			'&page=' +
			page.page +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
}
//채용공고리스트
function RecruitList(page) {
	return instance.get(
		'api/recruit/lists' +
			'?searchKeyword=' +
			page.searchKeyword +
			'&page=' +
			page.page +
			'&size=' +
			page.size,
		{
			headers,
		},
	);
}

//토큰 값 넘기기
const headers = {
	'Access-Control-Allow-Origin': '*',
	'Content-type': 'application/json',
	Accept: '*/*',
};
//멀티파트폼데이터 옵션추가
const headersFormdata = {
	'Access-Control-Allow-Origin': '*',
	'Content-type': 'multipart/form-data',
	Accept: '*/*',
};
function loginUser(userData) {
	// const qs = require('qs');
	return instance.post('api/auth/admin-login', JSON.stringify(userData), {
		headers,
	});
}

export {
	loginUser,
	BrandList,
	BrandSelect,
	categoryList,
	SnsList,
	fetchsns,
	editSns,
	fetchcategory,
	editcategory,
	createBrand,
	deleteBrand,
	editBrand,
	fetchBrand,
	NewsList,
	createdNews,
	createSns,
	fetchNews,
	editNews,
	RecruitList,
	createdRecruit,
	fetchRecruit,
	createcategory,
};
