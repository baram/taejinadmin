import Vue from 'vue';
import App from './App.vue';
import router from './routes/index';
import store from './store/index';
import VeeValidate from 'vee-validate';
// import './vee-validate';
import './css/default.css';

Vue.config.productionTip = false;

new Vue({
	render: h => h(App),
	router,
	store,
	VeeValidate,
}).$mount('#app');
