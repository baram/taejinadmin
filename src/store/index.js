import Vue from 'vue';
import Vuex from 'vuex';
// import mutations from './mutation.js';
// import actions from './actions.js';
import createPersistedState from 'vuex-persistedstate';
import { NewsList, RecruitList, BrandSelect } from '../api/index.js';

Vue.use(Vuex);

export default new Vuex.Store({
	plugins: [createPersistedState()],
	state: {
		token: '',
		isLogin: '',
		news: {},
		recruit: {},
		selectBand: {},
	},
	getters: {
		isLogin(state) {
			return state.isLogin !== '';
		},
	},
	mutations: {
		setIsLogin(state, loginYn) {
			state.isLogin = loginYn;
		},
		clearIsLogin(state) {
			state.isLogin = '';
		},
		setToken(state, token) {
			state.token = token;
		},
		clearToken(state) {
			state.token = '';
		},
		SET_NEWS(state, news) {
			state.news = news;
		},
		SET_RECRUIT(state, recruit) {
			state.recruit = recruit;
		},
		SET_BRAND_SELECT(state, select) {
			state.selectBand = select;
		},
	},
	actions: {
		async LIST_NEWS({ commit }, page) {
			const { data } = await NewsList(page);
			commit('SET_NEWS', data);
		},
		async LIST_RECRUIT({ commit }, page) {
			const { data } = await RecruitList(page);
			commit('SET_RECRUIT', data);
		},
		async SELECT_BRAND({ commit }) {
			const { data } = await BrandSelect();
			commit('SET_BRAND_SELECT', data);
		},
	},
});
